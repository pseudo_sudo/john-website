class StaticPagesController < ApplicationController
  before_filter :get_static_json


  def index
  end

  def project

      name = params[:name].downcase

      if name == "investorist" || name == "spekit" || name == "reverseasy"
        #set the project for the view
        @project = @projects[name]
        #set the title of the page
        @title = @projects[name]['title']
      else
        #if no project redirect to projects
        redirect_to :action => 'index', :anchor => 'projects'
      end

  end

  def experience
  end



  private
  def get_static_json
    projects_json = File.read(Rails.root.join('lib', 'projects.json'))
    @projects = JSON.parse(projects_json)

    timeline_json = File.read(Rails.root.join('lib', 'timeline.json'))
    @timeline = JSON.parse(timeline_json).sort_by { |e| e["time"]["start-year"]  }.reverse

    index_json = File.read(Rails.root.join('lib', 'index-content.json'))
    @index_content = JSON.parse(index_json)

  end

end
