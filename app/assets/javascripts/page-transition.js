
$(document).ready(function(){
    var wrapper = $('.content-wrapper');
    var isRoot = $('.container-root').length >0;

    $(document).on('page:before-change', function() {
      if(isRoot){
        wrapper.addClass('animated fadeOutLeftBig');
      } else{
          wrapper.addClass('animated fadeOutRightBig');
      }
    });
    $(document).on('page:fetch', function() {

              $('#site-loader').addClass('loading');
    });
    $(document).on('page:receive', function() {

    });
    $(document).on('page:before-unload', function() {

    });
    $(document).on('page:after-remove', function() {

    });

    $(document).on('page:change', function(ev) {

        if(typeof __insp !== 'undefined'){
          __insp.push(["virtualPage"]);
        }
        wrapper = $('.content-wrapper');
        isRoot = $('.container-root').length >0;


        wrapper.removeClass('animated fadeOutRightBig');
          wrapper.removeClass('animated fadeOutLeftBig');

        if(isRoot){

        } else {
            wrapper.addClass('animated fadeInRightBig');
        }

        $('#site-loader').removeClass('loading');

    });

});
