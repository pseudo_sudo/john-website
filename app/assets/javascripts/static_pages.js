// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

 $.fn.isOnScreen = function(){
     var win = $(window);

     var viewport = {
         top : win.scrollTop(),
         left : win.scrollLeft()
     };
     viewport.right = viewport.left + win.width();
     viewport.bottom = viewport.top + win.height();

     var bounds = this.offset();
     bounds.right = bounds.left + this.outerWidth();
     bounds.bottom = bounds.top + this.outerHeight();

     return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

 };

  $.fn.isCompletelyOnScreen = function(){

      var win = $(window);

      var viewport = {
          top : win.scrollTop(),
          left : win.scrollLeft()
      };
      viewport.right = viewport.left + win.width();
      viewport.bottom = viewport.top + win.height();

      var bounds = this.offset();
      bounds.right = bounds.left + this.outerWidth();
      bounds.bottom = bounds.top + this.outerHeight();

      return (!(viewport.right < bounds.right || viewport.left > bounds.left || viewport.bottom < bounds.bottom || viewport.top > bounds.top));

  };



function handleAnimations(){
  var hiddenElems = $('.animate-on-vis');
  hiddenElems.each(function(){
    var el = $(this);
    if(el.isOnScreen()){

      el.addClass(el.attr('data-animate-on-vis'));

      el.removeClass('animate-on-vis');
    }
  });
}


var navbar;
function handleNavbar(){
  //if viewport is at the top of scroller
  //console.log($('.index-hero img'));
  if($('.index-hero img').length > 0 && $('.index-hero img').isCompletelyOnScreen() && !$('.navbar-collapse').hasClass('in') ) {
    navbar.removeClass('navbar-default');
    navbar.addClass('navbar-inverse');
  } else {
      navbar.removeClass('navbar-inverse');
      navbar.addClass('navbar-default');
  }
}


function initProjectFlipEvents(){
  //enable close for touch events on projects
  var closeBtns = $('.project-card-flip .flip-card-back');
  closeBtns.on('touchstart', function(ev){

   var el = $(this).parents('.project-card-flip');
   //console.log(el);
   el.removeClass('hover');
   //console.log(el);

  });
}

function smoothScrollTo($target, target){
  console.log($target, target);
  $('html, body').stop().animate({
      'scrollTop': $target.offset().top - ($('nav').outerHeight() - 3)
  }, 250, 'swing', function(){

    if(target)window.location.hash = target;

    setTimeout(function(){
      $('html, body').stop().animate({
            'scrollTop': $target.offset().top - ($('nav').outerHeight() - 3)
      }, 20);
    }, 50);


  });
}

function initSmoothScroll(){

     //scrollTop
     $('a[href^="#"]').on('click',function (e) {
  	    e.preventDefault();

  	    var target = this.hash;
  	    var $target = $(target);
        if($target.length<1){
          return;
        }
        smoothScrollTo($target, target);
  	});
}

function scrollSpy(){
  var links = $($('nav .nav.navbar-nav li').get().reverse());

  links.each(function(){
    var link = $(this).find('a');
    var url = link.attr('href');

    if(url.indexOf('#') == -1){
      return;
    }
    var bodySelector = '#' + url.substring(url.indexOf('#')+1);
    var bodyElement = $(bodySelector);

    //if the div is more than 75% on the screen
    if(bodyElement.length < 1 ){
      return;
    }
    //
    var win = $(window);
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = bodyElement.offset();
    bounds.right = bounds.left + bodyElement.outerWidth();
    bounds.bottom = bounds.top + bodyElement.outerHeight();
    var navHeight = $('.navbar').height();
    //
    var elementActive = (!( ( viewport.top + navHeight ) > bounds.bottom - 100));





    if( elementActive ){
      //remove active on all navs
      links.removeClass('active');
      $(this).addClass('active');
    }
  });
}
function initIndexTypeAhead(){

       //function for doing the typing animation
       $("#title-subheading").typed({
          strings: [
          "^1000Web Developer. ^500",
          "^500Front End Web  Developer. ^500",
          "^500Ruby on Rails Developer.",
          "^Angular Developer.",
          "^500Software Developer. ^500",
          "^500Web ^1000 ...",
          "Full Stack Web Developer ^500<span class='devicons devicons-html5'>.</span>",

        ],
          typeSpeed: 20
        });
}

function fixTransparentNavbarCollapse(){

       //functions for handling the expanding and collapsing of the navbar with transparency
       $(".navbar-collapse").on('show.bs.collapse', function () {
               navbar.removeClass('navbar-inverse');
               navbar.addClass('navbar-default');
       });
       $(".navbar-collapse").on('hide.bs.collapse', function () {

         if($('.index-hero img').isCompletelyOnScreen() ) {
           navbar.removeClass('navbar-default');
           navbar.addClass('navbar-inverse');
         } else {
             navbar.removeClass('navbar-inverse');
             navbar.addClass('navbar-default');
         }
       });
}

var onScroll = function(){
    handleNavbar();     //make navbar transparent or not depending on position
    handleAnimations(); // animate on scroll if it has that effect
    scrollSpy();

    if($('#skills-experience').length>0 && $('#skills-experience').isOnScreen()){
      if($('svg.paralax').attr('class').indexOf('on') ==-1){
        $('svg.paralax').attr('class', $('svg.paralax').attr('class') + ' '+ 'on' );
      }
    }

    if($('.timeline-row').length>0){
      $('.timeline-row').each(function(){

        //if completely on screen set to active
        if($(this).isCompletelyOnScreen()){
          $(this).addClass('timeline-row-active');
        //else remove the active
        } else {
          $(this).removeClass('timeline-row-active');
        }
      });
    }

}

var ready = function(){
     navbar = $('.navbar');

    if(window.location.hash && $(window.location.hash).length >0) {
      smoothScrollTo($(window.location.hash));
    }
     //Fix so that when a project in clicked if a person clicks back they immediatly go to projects section
     var projectLinks = $('.projects .read-more');
     projectLinks.click(function(){
       window.location.hash = 'projects';
     });



      initProjectFlipEvents();

      initSmoothScroll();
      //apply the index typeahead
      initIndexTypeAhead();

      fixTransparentNavbarCollapse();

      onScroll();         //do the scroll events on page load
      $.material.init()


}

//run on page load
$(document).ready(ready)
$(document).on('page:load', ready);
//scroll handler
$(window).scroll(onScroll);
